package com.remcal.demo;

import com.remcal.utils.ShiroUtil;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import java.util.Scanner;

/**
 * @ClassName: Demo03
 * @Descirption: 使用shiroUtil工具简化操作
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 1/9/2020 11:25 AM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class Demo03 {

    public static void main(String[] args) {

        Subject subject = ShiroUtil.getSubject();
        if (!subject.isAuthenticated()) {

            Scanner scanner = new Scanner(System.in);
            System.out.println("Please type in a username！");
            String username = scanner.nextLine();
            System.out.println("Please type in a password！");
            String password = scanner.nextLine();

            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            try {
                subject.login(token);
            } catch (UnknownAccountException e) {
                System.out.println("账户错误！");
            } catch (IncorrectCredentialsException e) {
                System.out.println("密码错误！");
            }

        } else {
            System.out.println("Already in login status.");
        }

        if (subject.isAuthenticated()) {
            System.out.println("Login success!!!");
        } else {
            System.out.println("Login failure!!!");
        }
    }
}
