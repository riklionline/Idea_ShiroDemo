package com.remcal.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;

import java.util.Scanner;

/**
 * @ClassName: Demo01
 * @Descirption: 使用shiro安全框架进行模拟登录操作
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 1/8/2020 5:03 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class Demo01 {

    public static void main(String[] args) {

        //1、创建securityManager对象
        DefaultSecurityManager securityManager = new DefaultSecurityManager();

        //2、securityManager对象绑定realms数据源
        //2.1、创建realm对象（此案例为文本数据源ini）
        IniRealm realm = new IniRealm("classpath:shiro.ini");
        //2.2、将realm绑定在security上
        securityManager.setRealm(realm);

        //3、将安全工具类和securitymanager绑定
        SecurityUtils.setSecurityManager(securityManager);

        //4、从securityutils中获取subject对象
        Subject subject = SecurityUtils.getSubject();

        //5、开始认证
        //5.1 将输入信息装入usernamepasswordToken对象中
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please type in a username！");
            String username = scanner.nextLine();
            System.out.println("Please type in a password！");
            String password = scanner.nextLine();

            UsernamePasswordToken token = new UsernamePasswordToken(username, password);

            try {
                subject.login(token);
            } catch (UnknownAccountException e) {
                System.out.println("账户错误！");
            } catch (IncorrectCredentialsException e) {
                System.out.println("密码错误！");
            }

            System.out.println("登录状态？" + subject.isAuthenticated());
        }
    }

}
