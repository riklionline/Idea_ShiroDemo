package com.remcal.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;

import java.util.Scanner;

/**
 * @ClassName: Demo01
 * @Descirption: 使用shiro安全框架进行模拟1-登录、2-登出、3-角色认定、4-权限校验操作（使用了方法抽取技能Ctrl+Alt+M）
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 1/8/2020 5:03 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class Demo02 {

    public static void main(String[] args) {

        //1、创建securityManager对象
        DefaultSecurityManager securityManager = new DefaultSecurityManager();

        //2、securityManager对象绑定realms数据源
        //2.1、创建realm对象（此案例为文本数据源ini）
        IniRealm realm = new IniRealm("classpath:shiro.ini");
        //2.2、将realm绑定在security上
        securityManager.setRealm(realm);

        //3、将安全工具类和securitymanager绑定
        SecurityUtils.setSecurityManager(securityManager);

        //4、从securityutils中获取subject对象
        Subject subject = SecurityUtils.getSubject();

        //5、开始认证
        //5.1 将输入信息装入usernamepasswordToken对象中

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("press 1 login, press 2 logout, press 3 role, press 4 permission.");
            int key = scanner.nextInt();
            switch (key) {
                case 1:
                    login(subject);
                    break;
                case 2:
                    logout(subject);
                    break;
                case 3:
                    role(subject);
                    break;
                case 4:
                    permission(subject);
                    break;
            }
        }
    }

    /**
     * 判断当前输入的用户是否有指定的角色，并且已经为此角色指定了权限
     * @param subject
     */
    private static void permission(Subject subject) {
        if (subject.isAuthenticated()) {
            boolean isPermitted = subject.isPermitted("select");
            System.out.println("Has this permisstion?"+isPermitted);
        } else {
            System.out.println("You are not in login status！！！");
        }
    }

    /**
     * 判断当前输入的用户是否有指定的角色（在ini数据源文件中）
     * @param subject
     */
    private static void role(Subject subject) {
        if (subject.isAuthenticated()) {
            boolean hasRole = subject.hasRole("admin");
            System.out.println("Has this role？"+hasRole);
        } else {
            System.out.println("You are not in login status！！！");
        }
    }

    /**
     * 登出方法
     * @param subject
     */

    private static void logout(Subject subject) {
        if (subject.isAuthenticated()) {
            subject.logout();
        } else {
            System.out.println("Already in out status!");
        }
    }

    /**
     * 登录方法
     * @param subject
     */
    private static void login(Subject subject) {
        /**
         * 判断subject中的全部角色有没有任意一个在登录状态下，因为登录login(token)方法，的token一次只有一个，所以可以存在cookie里面，响应给浏览器
         * 而subject对象中虽然包含了数据源中的全部用户角色信息，但是一次就判断一个，因为一次就传进来一个token
         * subject的使用思路就是：一个用户拿到一个subject对象，然后用户拿着自己的身份证“token”交接subject询问subject有没有“我”，如果有就可以登录成功
         * 进一步引申比喻：一个人到提款机去取钱，自己携带一个“卡：token”，交给提款机“subject”，问提款机，我这个卡可以登录吗？
         * 所以：注册用户，应该早于登录完成，即先把自己的用户信息注册到数据源中
         */

        if (!subject.isAuthenticated()) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please type in a username！");
            String username = scanner.nextLine();
            System.out.println("Please type in a password！");
            String password = scanner.nextLine();

            UsernamePasswordToken token = new UsernamePasswordToken(username, password);

            try {
                subject.login(token);
            } catch (UnknownAccountException e) {
                System.out.println("账户错误！");
            } catch (IncorrectCredentialsException e) {
                System.out.println("密码错误！");
            }
            System.out.println("登录状态？" + subject.isAuthenticated());
        } else {
            System.out.println("Already in login status!");
        }
    }
}
