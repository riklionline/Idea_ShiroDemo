package com.remcal.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;

/**
 * @ClassName: ShiroUtil
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 1/9/2020 11:21 AM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class ShiroUtil {

    static {
        //1、创建securityManager对象
        DefaultSecurityManager securityManager = new DefaultSecurityManager();

        //2、securityManager对象绑定realms数据源
        //2.1、创建realm对象（此案例为文本数据源ini）
        IniRealm realm = new IniRealm("classpath:shiro.ini");
        //2.2、将realm绑定在security上
        securityManager.setRealm(realm);

        //3、将安全工具类和securitymanager绑定
        SecurityUtils.setSecurityManager(securityManager);
    }

    public static Subject getSubject(){
        Subject subject = SecurityUtils.getSubject();
        return subject;
    }
}
